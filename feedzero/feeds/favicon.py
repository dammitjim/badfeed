from collections import namedtuple
from dataclasses import dataclass
from tempfile import NamedTemporaryFile
from typing import IO, List, Union

import favicon
import feedparser
from furl import furl
import requests

from feedzero.feeds.models import Feed
from feedzero.ingest.constants import REQUESTS_USER_AGENT


FaviconFile = namedtuple("FaviconFile", ["file", "icon"])


class InvalidContentTypeException(Exception):
    pass


@dataclass
class FaviconFetcher:
    """Fetch the favicon for the given feed to be used as the logo.

    Tries all available endpoints to pull in the favicon from, one after
    another.
    """

    feed: Feed

    REQUEST_HEADERS = {"User-Agent": REQUESTS_USER_AGENT}

    def fetch(self) -> Union[FaviconFile, None]:
        """Return a written icon file or None if none can be resolved."""
        try_fns = [self._try_stated_link, self._try_rss_link, self._try_rss_origin]
        for fn in try_fns:
            icons = fn()
            if len(icons) == 0:
                continue
            icon = _get_optimal_icon(icons)

            try:
                downloaded_favicon = self._download_to_file(icon)
            except InvalidContentTypeException:
                continue

            return FaviconFile(downloaded_favicon, icon)

        return None

    def _try_stated_link(self) -> List[favicon.Icon]:
        """Attempt to pull favicon from link present in parsed rss feed."""
        response = requests.get(self.feed.link, headers=self.REQUEST_HEADERS)
        parsed_feed = feedparser.parse(response.text)
        link = parsed_feed.feed.link
        if not link:
            return []
        return favicon.get(link, headers=self.REQUEST_HEADERS)

    def _try_rss_link(self) -> List[favicon.Icon]:
        """Try the RSS link itself."""
        link = self.feed.link
        return favicon.get(link, headers=self.REQUEST_HEADERS)

    def _try_rss_origin(self) -> List[favicon.Icon]:
        """Try the origin URL of the feed."""
        link = furl(self.feed.link)
        return favicon.get(link.origin, headers=self.REQUEST_HEADERS)

    def _download_to_file(self, icon: favicon.Icon) -> IO:
        """Write to temporary file, return to be handled."""
        response = requests.get(icon.url, stream=True, headers=self.REQUEST_HEADERS)
        if not response.headers["content-type"].startswith("image"):
            raise InvalidContentTypeException("Response did not return an image.")

        lf = NamedTemporaryFile(suffix=f".{icon.format}", buffering=0)
        for chunk in response:
            lf.write(chunk)
        return lf


def _get_optimal_icon(icons: List[favicon.Icon]) -> favicon.Icon:
    """Try to match a square icon, if none can be found return the first.

    The first is the largest available icon (from the favicon lib.)
    """
    preferred_formats = ["png", "jpg", "jpeg"]
    return next(
        (i for i in icons if i.width == i.height and i.format in preferred_formats),
        icons[0],
    )
