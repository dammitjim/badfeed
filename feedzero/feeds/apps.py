from django.apps import AppConfig
from django_rq import get_scheduler

from feedzero.ingest.jobs import queue_feeds_for_sync


CRON_EXECUTE_EVERY_MINUTE = "*/1 * * * *"


class FeedsConfig(AppConfig):
    name = "feedzero.feeds"

    def ready(self):
        import feedzero.feeds.receivers  # noqa

        scheduler = get_scheduler("default")
        for job in scheduler.get_jobs():
            scheduler.cancel(job)

        scheduler.cron(
            CRON_EXECUTE_EVERY_MINUTE, func=queue_feeds_for_sync, repeat=None
        )
