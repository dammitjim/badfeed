import pathlib
import tempfile

from django.core import files
from django_rq import job
from furl import furl
from loguru import logger
from PIL import Image
import requests

from feedzero.feeds.favicon import FaviconFetcher


@job
def enrich_feed_with_favicon(feed):
    """Pull down favicon from available URL.

    TODO if any part of this fails, need a placeholder image.
    """
    fetcher = FaviconFetcher(feed)
    fetched_file = fetcher.fetch()
    if not fetched_file:
        # TODO this should probably just log
        raise ValueError(f"Unable to pull favicon for feed {feed.name}")

    im = Image.open(fetched_file.file.name)
    tf = tempfile.NamedTemporaryFile(suffix="png")
    im.thumbnail((192, 192))
    im.save(tf.name, "png")

    filename = f"{feed.slug}.{fetched_file.icon.format}"
    feed.logo.save(filename, files.File(tf))


@job
def enrich_entry_with_thumbnail(entry):
    """Pull down the top available image, thumbnail it."""
    enriched = entry.enriched
    if not len(enriched.images) > 0:
        return
    top_image_url = enriched.images[0]

    clean_file_url = furl(top_image_url).remove(args=True, fragment=True).url
    extension = pathlib.Path(clean_file_url).suffix

    response = requests.get(top_image_url, stream=True)
    if response.status_code != 200:
        logger.error("Non-200 status code received when pulling thumbnail.")
        return

    # pull down the original file, `buffering` is passed to the `open` call
    # for some reason buffering causes truncated images to occasionally be
    # downloaded so it's explicitly turned off here
    lf = tempfile.NamedTemporaryFile(suffix=extension, buffering=0)
    for chunk in response:
        lf.write(chunk)

    # create a thumbnail, save to a temp file
    im = Image.open(lf.name)
    im.thumbnail((600, 400))
    tf = tempfile.NamedTemporaryFile(suffix=extension)
    im.save(tf.name)

    # save to django model
    entry.thumbnail.save(f"{entry.feed.slug}__{entry.slug}.{extension}", files.File(tf))
