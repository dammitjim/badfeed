FROM python:3.7
ENV PYTHONUNBUFFERED 1

RUN apt-get install libjpeg62-turbo-dev
RUN apt-get install zlib1g-dev

RUN mkdir /code
WORKDIR /code
COPY Pipfile /code/
COPY Pipfile.lock /code/

RUN pip install pipenv
RUN pipenv install --dev --system --ignore-pipfile

COPY . /code/
