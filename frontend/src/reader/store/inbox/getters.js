const INBOX_PAGE_SIZE = 12;

export default {
  /**
   * Load the first X for the inbox.
   */
  inbox: state => {
    return state.entries.slice(0, INBOX_PAGE_SIZE);
  }
};
