import axios from "@/reader/store/axios";
import { normaliseEntry } from "@/reader/store/normalise";

export default {
  /**
   * Hit the API for the given page of results.
   */
  async fetch({ commit, state }, page = 1) {
    const response = await axios.get(`/api/v1/entries/?page=${page}`);
    if (response.status !== 200) {
      throw "fetch Non 200 response status received from API";
    }

    const entries = response.data.results.map(normaliseEntry);
    entries.forEach(entry => {
      if (state.entries.find(e => e.id === entry.id) !== undefined) {
        return;
      }
      commit("add", entry);
    });
  },

  /**
   * Persist done state to the API.
   */
  async persistDone({ commit, dispatch }, entries) {
    entries.forEach(entry => commit("remove", entry));
    const actions = entries.map(entry => {
      return {
        state: "deleted",
        entry_id: entry.id
      };
    });

    const response = await axios.post(`/api/v1/states/`, { actions });
    if (response.status !== 200) {
      throw "persistDone Non 200 response status received from API";
    }
    dispatch("fetch");
  },

  async remove({ commit }, entry) {
    commit("remove", entry);
  }
};
