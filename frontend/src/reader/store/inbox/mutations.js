export default {
  add(state, entry) {
    state.entries.push(entry);
  },
  remove(state, entry) {
    state.entries = state.entries.filter(e => e.id !== entry.id);
  }
};
