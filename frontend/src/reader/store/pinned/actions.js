import axios from "@/reader/store/axios";
import { normaliseEntry } from "../normalise";
import { apiActionsFactory } from "../utils";

export default {
  async persistPin({ commit, dispatch, state }, entry) {
    await commit("inbox/remove", entry, { root: true });
    const alreadyPinned =
      state.entries.find(e => e.id === entry.id) !== undefined;

    if (!alreadyPinned) {
      const body = { actions: apiActionsFactory([entry], "pinned") };
      const response = await axios.post(
        `http://localhost:8000/api/v1/states/`,
        body
      );

      if (response.status !== 200) {
        throw "Non 200 response status received from API";
      }

      commit("pin", entry);
    }

    await dispatch("inbox/fetch", 1, { root: true });
  },

  async fetch({ commit, state }, page = 1) {
    const response = await axios.get(`/api/v1/entries/pinned/?page=${page}`);
    if (response.status !== 200) {
      throw "Non 200 response status received from API";
    }

    response.data.results.forEach(entry => {
      if (state.entries.find(e => e.id === entry.id) !== undefined) {
        return;
      }
      commit("pin", normaliseEntry(entry));
    });
  }
};
