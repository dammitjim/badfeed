const PINNED_PAGE_SIZE = 6;

export default {
  /**
   * Load the first X for the inbox.
   */
  pinned: state => {
    return state.entries.slice(0, PINNED_PAGE_SIZE);
  }
};
