import Vue from "vue";
import Vuex from "vuex";

import inbox from "./inbox/store";
import pinned from "./pinned/store";
import feeds from "./feeds/store";

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    inbox,
    pinned,
    feeds
  }
});
