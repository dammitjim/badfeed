import axios from "../axios";
import { normaliseFeed } from "../normalise";

export default {
  async fetch({ commit, state }, page = 1) {
    const response = await axios.get(
      `/api/v1/feeds/?only_watched=true&page=${page}`
    );
    if (response.status !== 200) {
      throw "Non 200 response status received from API";
    }

    response.data.results.forEach(feed => {
      if (state.feeds.find(f => f.id === feed.id) !== undefined) {
        return;
      }
      commit("add", normaliseFeed(feed));
    });
  }
};
