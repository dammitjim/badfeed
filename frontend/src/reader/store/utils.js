export const apiActionFactory = (entry, actionType) => {
  return {
    state: actionType,
    entry_id: entry.id
  };
};

export const apiActionsFactory = (entries, actionType) => {
  return entries.map(entry => {
    return apiActionFactory(entry, actionType);
  });
};
